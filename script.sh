#!/usr/bin/env bash
echo "Deploy script started"
eval $(ssh-agent)
ssh-add bitbucket_ssh
cd /home/omorboshir/CICD/flaskcicd/
git pull
supervisorctl restart flaskcicd
exit
echo "Deploy script stopped"
